CREATE DATABASE healthcare_management;
--Connect to the newly created database (by selecting DBMS tool).

CREATE SCHEMA healthcare_schema;

CREATE TABLE healthcare_schema.Patient (
    patient_id SERIAL PRIMARY KEY,
    patient_name VARCHAR(100) NOT NULL,
    gender CHAR(1) CHECK (gender IN ('M', 'F', 'O')),
    date_of_birth DATE CHECK (date_of_birth > '2000-01-01')
);

CREATE TABLE healthcare_schema.Doctor (
    doctor_id SERIAL PRIMARY KEY,
    doctor_name VARCHAR(100) NOT NULL,
    specialization VARCHAR(100) NOT NULL
);

CREATE TABLE healthcare_schema.Appointment (
    appointment_id SERIAL PRIMARY KEY,
    appointment_date DATE NOT NULL CHECK (appointment_date >= CURRENT_DATE),
    patient_id INT REFERENCES healthcare_schema.Patient(patient_id),
    doctor_id INT REFERENCES healthcare_schema.Doctor(doctor_id)
);

CREATE TABLE healthcare_schema.Measurement (
    measurement_id SERIAL PRIMARY KEY,
    value DECIMAL NOT NULL CHECK (value >= 0),
    measured_date DATE NOT NULL CHECK (measured_date >= '2000-01-01'),
    patient_id INT REFERENCES healthcare_schema.Patient(patient_id)
);


ALTER TABLE healthcare_schema.Patient
ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp;

ALTER TABLE healthcare_schema.Doctor
ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp;

ALTER TABLE healthcare_schema.Appointment
ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp;

ALTER TABLE healthcare_schema.Measurement
ADD COLUMN record_ts TIMESTAMP DEFAULT current_timestamp;

INSERT INTO healthcare_schema.Patient (patient_name, gender, date_of_birth) 
VALUES 
('John Doe', 'M', '1990-05-15'),
('Jane Smith', 'F', '1985-10-20');

INSERT INTO healthcare_schema.Doctor (doctor_name, specialization) 
VALUES 
('Dr. Smith', 'Cardiology'),
('Dr. Johnson', 'Pediatrics');

INSERT INTO healthcare_schema.Appointment (appointment_date, patient_id, doctor_id) 
VALUES 
('2024-04-03', 1, 1),
('2024-04-05', 2, 2);

INSERT INTO healthcare_schema.Measurement (value, measured_date, patient_id) 
VALUES 
(150.5, '2024-04-01', 1),
(65.2, '2024-03-30', 2);

SELECT * FROM healthcare_management.Patient;
SELECT * FROM healthcare_management.Doctor;